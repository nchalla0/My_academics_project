import java.awt.Font;
import javax.swing.JTextArea;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JOptionPane;
import java.io.File;
import java.net.*;
import javax.swing.SwingUtilities;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import java.awt.Button;
import javax.swing.JLabel;
import java.util.Random;
import java.io.InputStream;
import java.io.FileNotFoundException;

class InThread extends Thread{
		private int bytesFields, bytesData, response;
		private DatagramSocket socket;
		private PrintWriter file;	
		private boolean finalF, finalTT;
		private InetAddress inetAddress;
		private int incomingPort=0;

		InThread(DatagramSocket socket){
			this.socket = socket;
			this.response = 0;
			this.bytesFields = 48;
			this.bytesData = 528;
			this.finalF = false;
			this.finalTT = false;
		}

		private void submit(DatagramPacket in, int seq, int type){
			try {
				byte[] msn = new byte[bytesFields];
				String varAux = InetAddress.getLocalHost().getHostAddress();
				byte[] varAdddress = ByteBuffer.allocate(16).put(varAux.getBytes()).array();
				for (int j = 0; j<16; j++)  msn[j] = varAdddress[j];	

				String varAux2 = in.getAddress().getHostAddress();	
				byte[] varDest = ByteBuffer.allocate(16).put(varAux2.getBytes()).array();
				for (int j = 0; j<16; j++)  msn[j+16] = varDest[j];

				byte[] varSource = ByteBuffer.allocate(4).putInt(socket.getLocalPort()).array();
				for (int i = 0; i < 4; i++) msn[i+32] = varSource[i];

				byte[] varPort = ByteBuffer.allocate(4).putInt(in.getPort()).array();
				for (int i = 0; i < 4; i++)	msn[i+36] = varPort[i];

				byte[] varNext = ByteBuffer.allocate(4).putInt(seq).array();
				for (int i = 0; i < 4; i++)	msn[i+40] = varNext[i];


				byte[] varType = ByteBuffer.allocate(4).putInt(type).array();
				for (int i = 0; i < 4; i++) msn[i+44] = varType[i];
				DatagramPacket answerDatagram = new DatagramPacket(msn, msn.length, in.getAddress(), in.getPort());
				socket.send(answerDatagram);

			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}

		public synchronized void setAddress(InetAddress address){
			this.inetAddress = address;
			notify();
		}
		
		public synchronized InetAddress getAddress(){
			try {
				wait();
			}catch(InterruptedException ex){
				ex.printStackTrace();
	        }
			return inetAddress;
		}
		
		private int[] receive(byte[] packet){
			int[] result = new int[2];
			
			byte[] address1 = new byte[16]; for(int j = 0; j< 16; j++) address1[j] = packet[j];
			byte[] address2 = new byte[16]; for(int j = 0; j< 16; j++) address2[j] = packet[j+16];
			byte[] port1 = new byte[4]; for(int j = 0; j< 4; j++) port1[j] = packet[j+32];
			byte[] port2 = new byte[4]; for(int j = 0; j< 4; j++) port2[j] = packet[j+36];
			byte[] seq = new byte[4]; for(int j = 0; j< 4; j++) seq[j] = packet[j+40];
			byte[] type = new byte[4]; for(int j = 0; j< 4; j++) type[j] = packet[j+44];
			
			result[0] = ByteBuffer.wrap(seq).getInt();
			result[1] = ByteBuffer.wrap(type).getInt();

			return result;
		}
		
		public void run() {
			int prox = 0;
			try {
				file = new PrintWriter("DataRecieved.txt","UTF-8");
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println(e);
				System.exit(1);
			}
			
			byte[] buffer = new byte[bytesFields + bytesData];
			DatagramPacket datagram = new DatagramPacket(buffer, buffer.length);

			while(!finalTT || !finalF) {
				
					try {
						
						socket.receive(datagram);
						byte[] packet = datagram.getData();
						byte[] data = new byte[bytesData];
						int[] result = receive(packet);
						int seq = result[0];
						int inType = result[1];
						
						if ( inType == 1 && !finalF){
							
							if (seq==-2){
								prox = 0;
								finalF = true;
								submit(datagram, -2, 2);

							}else if (seq == prox){
								setPort(datagram.getPort());
								setAddress(datagram.getAddress());
								
								for (int i = bytesFields ; i < bytesData+bytesFields ; i++)
									data[i-bytesFields] = packet[i];
								
								String message = new String(data);
								file.print(message.replaceAll("\00", ""));
								prox = prox + bytesData;
								submit(datagram, prox, 2);

							}else{
								submit(datagram, prox, 2);
							}
							
						}else if ( inType == 2 && !finalTT){
							if ( seq == -2){
								finalTT = true;
								this.response = seq;
							}
							if ((seq > this.response) && (seq % bytesData == 0))
								this.response = seq;
						}
						
					} catch (IOException e) {
						e.printStackTrace();
						System.out.println(e);
					}
			}
			file.close();
		}

		public void setPort(int port){
			this.incomingPort = port;
		}
		
		public int getPort(){
			return incomingPort;
		}
		
		public int getResponse(){
			return this.response;
		}
}


class GoBackN extends JFrame {

	private static int frame, bytesFields, bytesData, varlos, timeout, counter, messageLost;
	private static InetAddress inetAddress;
	private static int port, serverPort=0 ;
	private static DatagramSocket socket;
	private JTextField text1, text2, text3;
	private static long bhghyu, finallyT;
	private static File inputFile;
	private static JTextArea show;
	

	public void dispatch(int seq, int type, byte[][] packetArray, int index){
		try{

			byte[] byteRes  = new byte[bytesFields+bytesData];
			String address = InetAddress.getLocalHost().getHostAddress();
			byte[] byteAddress = ByteBuffer.allocate(16).put(address.getBytes()).array();
			for (int i = 0; i<16; i++) byteRes[i] = byteAddress[i];

			String address2 = inetAddress.getHostAddress();
			byte[] destiny = ByteBuffer.allocate(16).put(address2.getBytes()).array();
			for (int i = 0; i<16; i++) byteRes[i+16] = destiny[i];

			byte[] bytePort1 = ByteBuffer.allocate(4).putInt(socket.getLocalPort()).array();
			for (int i = 0; i < 4; i++) byteRes[i+32] = bytePort1[i];

			byte[] bytePort2 = ByteBuffer.allocate(4).putInt(port).array();
			for (int i = 0; i < 4; i++) byteRes[i+36] = bytePort2[i];

			byte[] byteSeq = ByteBuffer.allocate(4).putInt(seq).array();
			for (int i = 0; i < 4; i++) byteRes[i+40] = byteSeq[i];

			byte[] byteType = ByteBuffer.allocate(4).putInt(type).array();
			for (int i = 0; i < 4; i++) byteRes[i+44] = byteType[i];

			if ( packetArray != null ){
				for(int j = bytesFields; j < bytesData+bytesFields ; j++)
					byteRes[j] = packetArray[index][j-bytesFields];
				String msg = new String(packetArray[index]);
				System.out.println(index +"= " +msg+"\n");
			}

		
			DatagramPacket answerDatagram = new DatagramPacket(byteRes, byteRes.length, inetAddress,port);
			socket.send(answerDatagram);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void dispatchFile(){
		try {
		
			inputFile = new File("COSC635_P2_DataSent.txt");
			InputStream input = new FileInputStream(inputFile);
			int lengthOfFile = (int) inputFile.length();

			if(serverPort!=0) socket = new DatagramSocket(serverPort);
			else socket = new DatagramSocket();
			byte[][] bytePacket =  new byte [frame][bytesData];
			
			int prox = 0;
			counter = 0;
			int windowInit = 0;
			int next = 0;
			

			InThread inThread = new InThread(socket);
			inThread.start();
			
			finallyT = System.currentTimeMillis();
			while (prox < lengthOfFile){
				if (next >= prox) {
					prox = next;
					int numPacket = frame;
					int end = 0;
					int leftover = 0;

					if ( prox != windowInit && ((prox - windowInit)/bytesData != frame)){
						int aux = (prox - windowInit)/bytesData;
						end = frame - aux;
						for (int i = 0 ; i < end ; i ++){
							bytePacket[i] = bytePacket[aux];
							aux++;
							leftover++;
						}
					}

					end = frame - end;
					if ( (prox == 0 && counter == 0) || prox != windowInit ){
						for (int i = 0 ; i < end ; i ++){
							byte[] buffer = new byte[bytesData];
							int read = 0;
							if ((read = input.read(buffer)) != -1 ){
								bytePacket[leftover] = buffer;
								leftover++;
							}else{
								if (leftover < frame) bytePacket[leftover] = null;
								numPacket = leftover;
								break;
							}
						}
					}

					
					windowInit = prox;
					int numSequence = windowInit;
					
					if (port == 0){
						inetAddress= inThread.getAddress();
						port = inThread.getPort();
						
					}
					
					Random generator = new Random(System.currentTimeMillis());
					int randomInt = generator.nextInt(100);
					
					if ( randomInt >= messageLost){
						for (int i = 0 ; i < numPacket ; i ++ ){
							if (bytePacket[i] == null){
								break;
							}else{
								dispatch(numSequence, 1, bytePacket, i);
								numSequence = numSequence + bytesData;
							}
						}
					}else
						varlos++;
				}

				try{
				    Thread.sleep(timeout*1000);
				
				}catch(InterruptedException e){
					e.printStackTrace();
					System.out.println("Error with Thread.sleep");
					System.exit(1);
				}

				next = inThread.getResponse();
				counter++;
			}
			
			bhghyu = System.currentTimeMillis();
			
			while(inThread.getResponse() != -2){
				dispatch( -2, 1, null, 0);

				try{
				    Thread.sleep(timeout*1000);
				
				}catch(InterruptedException e){
					e.printStackTrace();
					System.out.println("Error with Thread.sleep");
					System.exit(1);
				}
			}
			
			input.close();
			
		} catch (SocketException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e);
			System.exit(1);
		}
	}

	String getTime(){
		int time = ((int)(bhghyu - finallyT)/1000);
		int days = time / 86400;
		int res = time % 86400;
		int hours = res / 3600;
		res = res % 3600;
		int min = res / 60;
		int sec = res % 60;
		String result = days+" day, "+hours+" hours, "+min+" minutes and "+sec +" seconds";
		return result;
	}

	String getSentPackets(){
		return "" + counter * frame;
	}

	String getLostPackets(){
		return "" + varlos * frame;
	}

	public GoBackN(){
		super("GoBackN");
		this.frame = 8;
		this.bytesFields = 48;
		this.bytesData = 528;
		this.port=0;
		this.serverPort=0 ;
		this.varlos = 0;
		this.timeout = 1;
		
		setLayout(null);
		
		JLabel label1 = new JLabel("IP Address ");
		label1.setBounds(10, 5, 150, 30);

		label1.setFont(new Font("Serif", Font.ITALIC, 12));
		add(label1);
		text1 = new JTextField("");
		text1.setBounds(92, 13, 130, 15);
		text1.setFont(new Font("Serif", Font.ITALIC, 12));
		add(text1);
		
		JLabel label2 = new JLabel("Port");
		label2.setBounds(250, 5, 100, 30);
		label2.setFont(new Font("Serif", Font.ITALIC, 12));
		add(label2);
		text2 = new JTextField("");
		text2.setBounds(287, 13, 55, 15);
		text2.setFont(new Font("Serif", Font.ITALIC, 12));
		add(text2);
		
		JLabel label3 = new JLabel("%");
		label3.setBounds(405, 5, 30, 30);
		label3.setFont(new Font("Serif", Font.ITALIC, 12));
		add(label3);
		text3 = new JTextField("");
		text3.setBounds(370, 13, 30, 15);
		text3.setFont(new Font("Serif", Font.ITALIC, 12));
		add(text3);


		JLabel label4 = new JLabel("Report:");
		label4.setBounds(10, 40, 60, 30);
		label4.setFont(new Font("Serif", Font.ITALIC, 12));
		add(label4);

		show = new JTextArea("");
		show.setBounds(70,50,420,50);
		show.setEditable(false);
		show.setLineWrap(true);
		show.setFont(new Font("Serif", Font.ITALIC, 12));
		show.setWrapStyleWord(true);
		add(show);

		
		Button buttonStart = new Button("Send");
		buttonStart.setBounds(455,5,55,25);
		buttonStart.setFont(new Font("Serif", Font.ITALIC, 12));
		add(buttonStart);
		
		setSize(520,160);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	    buttonStart.addActionListener(new ActionListener() {

		    public void actionPerformed(ActionEvent e) {
	    		try {
	    			int txtPort = Integer.parseInt(text2.getText());
		    		int lossesP = Integer.parseInt(text3.getText());
		    		String address = text1.getText();
		    		

		    		if (address.equalsIgnoreCase("")){
						serverPort = txtPort;
						messageLost = lossesP;
						dispatchFile();

					}else{
						try {
							inetAddress = InetAddress.getByName(address);
							port = txtPort;
							messageLost = lossesP;
							dispatchFile();
						} catch (UnknownHostException exc) {
							JOptionPane.showMessageDialog(null,
						    	"Invalid address",
						    	"Error",
						    	JOptionPane.ERROR_MESSAGE);
						}
					}

					String report =  getSentPackets() +" packages were sent, of which ";
					report += getLostPackets() +" were lost packages. This took "+ getTime();

					show.setText(report);

		    		/*JOptionPane.showMessageDialog(null,
						    getSentPackets() + "\n" + getLostPackets() + "\n" + getTime(),
						    "Results",JOptionPane.INFORMATION_MESSAGE);*/


		    	} catch (NumberFormatException excep) {
		    		JOptionPane.showMessageDialog(null,
						    "Invalid port or invalid % packet losses. Port must be an integer between 1024 and 49151 and packet losses must be an integer between 0 and 99",
						    "Error", JOptionPane.ERROR_MESSAGE);
		    	}

		    	
		    }
		});		
	}


	public static void main(String[] args){
		GoBackN gbn = new GoBackN();
	}
	
}
	
